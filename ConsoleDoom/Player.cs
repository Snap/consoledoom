﻿using System.Numerics;

namespace ConsoleDoom
{
    /// <summary>
    /// Player info.
    /// </summary>
    public class Player
    {
        public string Name;

        public Vector2 Position;

        public float Angle;

        public Player(string name)
        {
            Name = name;
        }
    }
}

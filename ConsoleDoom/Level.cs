﻿using System;
using System.Diagnostics;
using System.Numerics;
using System.Text;

namespace ConsoleDoom
{
    /// <summary>
    /// Level for find wall and other objects by raycast.
    /// </summary>
    public class Level
    {
        private int _width;
        private int _height;

        private char[,] _level;
        private StringBuilder _initLevelString;

        public int Height => _height;

        public int Width => _width;

        public Level()
        {

        }

        /// <summary>
        /// Initialization level.
        /// </summary>
        public void Load()
        {
            _initLevelString = GetDevLevel();
            _width = 32;
            _height = 32;
            _level = new char[_width, _height];

            Reset();
        }

        /// <summary>
        /// Reset cell to init level.
        /// </summary>
        public void Reset()
        {
            for (int index = 0; index < _initLevelString.Length; index++)
            {
                var position = IndexToVector(index);
                _level[(int)position.X, (int)position.Y] = _initLevelString[index];
            }
        }

        /// <summary>
        /// Get cell by vector2 position.
        /// </summary>
        /// <param name="position">Cell position.</param>
        public char GetCell(Vector2 position)
        {
            return _level[(int)position.X, (int)position.Y];
        }

        /// <summary>
        /// Get cell by vector2 position.
        /// </summary>
        /// <param name="position">Cell position.</param>
        /// <param name="cellChar">Сell char.</param>
        public void SetCell(Vector2 position, char cellChar)
        {
            _level[(int)position.X, (int)position.Y] = cellChar;
        }

        /// <summary>
        /// Get spawn for player number or common player spawn.
        /// </summary>
        /// <param name="playerNumber">Player number.</param>
        /// <returns>Spawn position.</returns>
        public Vector2 GetPlayerSpawnPosition(int playerNumber)
        {
            var playerCharNumber = (char)(48 + playerNumber);
            var level = _initLevelString.ToString();

            var index = level.IndexOf(playerCharNumber);

            if (index == -1)
            {
                index = level.IndexOf(Const.CommonPlayerSpawn);

                if (index == -1)
                {
                    return Vector2.Zero;
                }
            }

            return IndexToVector(index);
        }

        /// <summary>
        /// Get init level as string.
        /// </summary>
        /// <returns>String level without newline.</returns>
        public string GetInitLevelString()
        {
            return _initLevelString.ToString();
        }

        /// <summary>
        /// Copy level buffer to destination array.
        /// </summary>
        /// <param name="destinationBuffer">Destination array buffer.</param>
        public void CopyTo(char[,] destinationBuffer)
        {
            Array.Copy(_level, 0, destinationBuffer, 0, _level.Length);
        }

        /// <summary>
        /// Convert string index to cell position vector2.
        /// </summary>
        /// <param name="index">String index.</param>
        private Vector2 IndexToVector(int index)
        {
            var y = index / _width;
            var x = index - y * _height;

            return new Vector2(x, y);
        }

        /// <summary>
        /// Hardcode level string for development.
        /// </summary>
        private static StringBuilder GetDevLevel()
        {
            var sb = new StringBuilder();
            sb.Append("################################");
            sb.Append("#............#.................#");
            sb.Append("#.....V......#.................#");
            sb.Append("#............#.................#");
            sb.Append("#.....0......#.............#...#");
            sb.Append("#............#.............#...#");
            sb.Append("#.....S......#.............#...#");
            sb.Append("##.........................#####");
            sb.Append("##.............................#");
            sb.Append("##.............................#");
            sb.Append("#........##....................#");
            sb.Append("#..............................#");
            sb.Append("#..............................#");
            sb.Append("#..#.......BB..................#");
            sb.Append("#..............................#");
            sb.Append("#.....................BBB......#");
            sb.Append("#.....................BBB......#");
            sb.Append("#..............................#");
            sb.Append("#..............................#");
            sb.Append("#..............................#");
            sb.Append("#..............................#");
            sb.Append("#####..######.......########...#");
            sb.Append("#...............#..............#");
            sb.Append("#...............#.#####.#......#");
            sb.Append("#...............#.#...#.#......#");
            sb.Append("#...............#.#.#.#.#......#");
            sb.Append("#...............#.#.#.#.#......#");
            sb.Append("#.........##....#.#...#.#......#");
            sb.Append("#..........#....#.#.#.#.#......#");
            sb.Append("#...............#.#.#.#.#......#");
            sb.Append("#...............#...#...#......#");
            sb.Append("################################");

            return sb;
        }
    }
}

﻿using System;

namespace ConsoleDoom
{
    /// <summary>
    /// All game constants.
    /// </summary>
    public static class Const
    {
        public const int ScreenWidth = 200;
        public const int ScreenHeight = 60;

        public const float CameraDepth = 16f;
        public const float CameraFov = MathF.PI / 3.5f;

        public const float PlayerWalkingSpeed = 3f;
        public const float PlayerRotationSpeed = 1.5f;
        public const float PlayerRunSpeed = 2f;

        public static readonly char[] ColissionBlocks = new char[] { '#', 'B' };

        public const char CommonPlayerSpawn = 'S';
        public const char MiniMapPlayerMarker = 'P';
        public const char MiniMapOutsideMarker = ' ';
        public const char MiniMapBorderHorizontal = '═';
        public const char MiniMapBorderVertical = '║';
        public static readonly char[] MiniMapBorderCorners = new char[] { '╔', '╗', '╚', '╝' };
    }
}

﻿using System;

namespace ConsoleDoom
{
    /// <summary>
    /// Console screen for draw graphics.
    /// </summary>
    public class Screen
    {
        private readonly int _screenWidth;
        private readonly int _screenHeight;
        private readonly char[] _buffer;

        public Screen(int width, int height)
        {
            _screenWidth = width;
            _screenHeight = height;
            _buffer = new char[_screenWidth * _screenHeight];
        }

        public void Init()
        {
            Console.SetWindowSize(_screenWidth, _screenHeight);
            Console.SetBufferSize(_screenWidth, _screenHeight);
            Console.CursorVisible = false;
        }

        public void Update()
        {
            Console.SetCursorPosition(0, 0);
            Console.Write(_buffer, 0, _screenWidth * _screenHeight);
        }

        public void CopyToBuffer(char[] array, int index)
        {
            array.CopyTo(_buffer, index);
        }

        public void SetPixel(int x, int y, char pixel)
        {
            _buffer[y * _screenWidth + x] = pixel;
        }
    }
}

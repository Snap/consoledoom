﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleDoom
{
    // source
    // https://www.youtube.com/watch?v=tR30963rDig
    // https://www.dropbox.com/s/052zwffcew3jn2b/Game3.zip

    public class Game
    {
        private readonly Screen _screen;
        private readonly Level _level;
        private readonly Player _player;
        private readonly PlayerController _playerController;
        private readonly SoundController _soundController;
        private readonly MiniMap _miniMap;
        private readonly Gui _gui;

        public Game()
        {
            _screen = new Screen(Const.ScreenWidth, Const.ScreenHeight);
            _level = new Level();
            _player = new Player("main");
            _playerController = new PlayerController(_player, _level);
            _soundController = new SoundController();
            _miniMap = new MiniMap(_screen, _player, 31, 16);
            _gui = new Gui(_screen, _player);
        }

        public void Run()
        {
            _screen.Init();
            _level.Load();
            _miniMap.Init(_level);

            _soundController.PlayMain();

            var mainPlayerPos = _level.GetPlayerSpawnPosition(0);
            _player.Position = mainPlayerPos;
            _player.Angle = 0f;

            float deltaTime = 0;
            var watch = Stopwatch.StartNew();

            while (true)
            {
                if (Console.KeyAvailable)
                {
                    if (Keyboard.IsKeyPressed(ConsoleKey.Escape))
                    {
                        break;
                    }

                    _playerController.Update(deltaTime);
                }

                //Ray Casting
                Parallel.For(0, Const.ScreenWidth, CastRay);

                _gui.Update(deltaTime);
                _miniMap.Update();
                _screen.Update();

                deltaTime = (float)watch.Elapsed.TotalSeconds;
                watch.Restart();
            }
        }

        public void CastRay(int x)
        {
            var rayAngle = (_player.Angle + Const.CameraFov / 2f) - x * Const.CameraFov / Const.ScreenWidth;

            float distanceToWall = 0;
            bool hitWall = false;
            bool isBound = false;
            float wallSize = 1;

            var rayPos = new Vector2(MathF.Sin(rayAngle), MathF.Cos(rayAngle));

            while (!hitWall && distanceToWall < Const.CameraDepth)
            {
                distanceToWall += 0.1f;

                var checkRayPos = _player.Position + rayPos * distanceToWall;

                if (checkRayPos.X < 0 
                    || checkRayPos.X >= Const.CameraDepth + _player.Position.X 
                    || checkRayPos.Y < 0 
                    || checkRayPos.Y >= Const.CameraDepth + _player.Position.Y)
                {
                    hitWall = true;
                    distanceToWall = Const.CameraDepth;
                }
                else
                {
                    char testCell = _level.GetCell(checkRayPos);

                    if (testCell == '#' || testCell == 'B')
                    {
                        hitWall = true;

                        wallSize = testCell == '#' ? 1f : 1.2f;

                        var boundsVectorsList = new List<Vector2>();

                        for (int tx = 0; tx < 2; tx++)
                        {
                            for (int ty = 0; ty < 2; ty++)
                            {
                                var vx = (int)checkRayPos.X + tx - _player.Position.X;
                                var vy = (int)checkRayPos.Y + ty - _player.Position.Y;

                                var vectorModule = MathF.Sqrt(vx * vx + vy * vy);
                                var cosAngle = (rayPos.X * vx / vectorModule) + (rayPos.Y * vy / vectorModule);
                                boundsVectorsList.Add(new Vector2(vectorModule, cosAngle));
                            }
                        }

                        boundsVectorsList = boundsVectorsList.OrderBy(v => v.X).ToList();

                        var boundAngle = 0.03f / distanceToWall;

                        if (MathF.Acos(boundsVectorsList[0].Y) < boundAngle ||
                            MathF.Acos(boundsVectorsList[1].Y) < boundAngle)
                            isBound = true;
                    }
                    else
                    {
                        _miniMap.SetMarker(checkRayPos, '+');
                    }
                }
            }

            int ceiling = (int)(Const.ScreenHeight / 2.0f - Const.ScreenHeight * Const.CameraFov / (distanceToWall * MathF.Cos(rayAngle - _player.Angle)));
            int floor = Const.ScreenHeight - ceiling;

            ceiling += (int)(Const.ScreenHeight - Const.ScreenHeight * wallSize);

            char wallShade;

            if (isBound)
                wallShade = '|';
            else if (distanceToWall <= Const.CameraDepth / 4.0f)
                wallShade = '\u2588';
            else if (distanceToWall < Const.CameraDepth / 3.0f)
                wallShade = '\u2593';
            else if (distanceToWall < Const.CameraDepth / 2.0f)
                wallShade = '\u2592';
            else if (distanceToWall < Const.CameraDepth)
                wallShade = '\u2591';
            else
                wallShade = ' ';

            for (int y = 0; y < Const.ScreenHeight; y++)
            {
                if (y < ceiling)
                    _screen.SetPixel(x, y, ' ');
                else if (y > ceiling && y <= floor)
                {
                    _screen.SetPixel(x, y, wallShade);
                }
                else
                {
                    char floorShade;
                    var b = 1.0f - (y - Const.ScreenHeight / 2.0f) / (Const.ScreenHeight / 2.0f);

                    if (b < 0.25f)
                        floorShade = '≡';
                    else if (b < 0.4f)
                        floorShade = '=';
                    else if (b < 0.6f)
                        floorShade = '-';
                    else if (b < 0.9f)
                        floorShade = '.';
                    else
                        floorShade = ' ';

                    _screen.SetPixel(x, y, floorShade);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleDoom.Sounds
{
    public abstract class BaseSound
    {
        public abstract void Play();
    }
}

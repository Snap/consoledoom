﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleDoom
{
    /// <summary>
    /// Draw UI, stats, hp, etc.
    /// </summary>
    public class Gui
    {
        private readonly Screen _screen;
        private readonly Player _mainPlayer;

        private readonly StringBuilder _stats;

        private int _maxFPS;
        private int _minFPS = int.MaxValue;
        private readonly Stopwatch _watchFps = Stopwatch.StartNew();

        public Gui(Screen screen, Player mainPlayer)
        {
            _screen = screen;
            _mainPlayer = mainPlayer;
            _stats = new StringBuilder();
        }

        public void Update(float deltaTime)
        {
            UpdateStats(deltaTime);
        }

        private void UpdateStats(float deltaTime)
        {
            var fps = 1 / deltaTime;

            if (_watchFps.Elapsed.TotalSeconds > 5)
            {
                _watchFps.Restart();
                _minFPS = (int)MathF.Min(_minFPS, fps);
                _maxFPS = (int)MathF.Max(_maxFPS, fps);
            }

            _stats.Clear();
            _stats.Append("X: ");
            _stats.Append($"{_mainPlayer.Position.X:0.000} ");
            _stats.Append("Y: ");
            _stats.Append($"{_mainPlayer.Position.Y:0.000} ");
            _stats.Append("A: ");
            _stats.Append($"{_mainPlayer.Angle:0.000} ");
            _stats.Append("FPS: ");
            _stats.Append((int)fps);
            _stats.Append(" (max: ");
            _stats.Append(_maxFPS);
            _stats.Append(", min: ");
            _stats.Append(_minFPS);
            _stats.Append(")");

            _screen.CopyToBuffer(_stats.ToString().ToCharArray(), 0);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ConsoleDoom.Sounds;

namespace ConsoleDoom
{
    public class SoundController
    {
        private Dictionary<string, Thread> _threadSounds = new Dictionary<string, Thread>();

        public SoundController()
        {

        }

        public void PlayMain()
        {
            PlaySound(new MainSound());
        }

        public void StopAll()
        {
            foreach (var sound in _threadSounds)
            {
                if (sound.Value.IsAlive)
                {
                    sound.Value.Interrupt();
                }
            }
        }

        private void PlaySound(BaseSound sound)
        {
            var threadSound = new Thread(x => sound.Play());
            threadSound.IsBackground = true;
            _threadSounds.Add(sound.GetType().Name, threadSound);
            threadSound.Start();
        }
    }
}

﻿using System;
using System.Threading.Tasks;

namespace ConsoleDoom
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var game = new Game();

            game.Run();
        }
    }
}

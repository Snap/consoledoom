﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleDoom
{
    public class Keyboard
    {
		[DllImport("user32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool GetKeyboardState(byte[] lpKeyState);

		public static int GetKeyState()
		{
			byte[] keys = new byte[256];

			//Get pressed keys
			if (!GetKeyboardState(keys))
			{
				int err = Marshal.GetLastWin32Error();
				throw new Win32Exception(err);
			}

			for (int i = 0; i < 256; i++)
			{

				byte key = keys[i];

				//Logical 'and' so we can drop the low-order bit for toggled keys, else that key will appear with the value 1!
				if ((key & 0x80) != 0)
				{

					//This is just for a short demo, you may want this to return
					//multiple keys!
					return (int)key;
				}
			}
			return -1;
		}

		[DllImport("user32.dll")]
		static extern short GetKeyState(ConsoleKey nVirtKey);

		public static bool IsKeyPressed(ConsoleKey testKey)
		{
			bool keyPressed = false;
			short result = GetKeyState(testKey);

			switch (result)
			{
				case 0:
					// Not pressed and not toggled on.
					keyPressed = false;
					break;

				case 1:
					// Not pressed, but toggled on
					keyPressed = false;
					break;

				default:
					// Pressed (and may be toggled on)
					keyPressed = true;
					break;
			}

			return keyPressed;
		}



		private const uint MAPVK_VK_TO_CHAR = 2;

		[System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
		static extern uint MapVirtualKeyW(uint uCode, uint uMapType);

		public static char KeyToChar(ConsoleKey key)
		{
			return unchecked((char)MapVirtualKeyW((uint)key, MAPVK_VK_TO_CHAR)); // Ignore high word.  
		}
	}
}

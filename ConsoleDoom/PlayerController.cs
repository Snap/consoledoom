﻿using System;
using System.Linq;
using System.Numerics;

namespace ConsoleDoom
{
    /// <summary>
    /// Player control.
    /// </summary>
    public class PlayerController
    {
        private readonly Player _player;
        private readonly Level _level;

        public PlayerController(Player player, Level level)
        {
            _player = player;
            _level = level;
        }

        public void Update(float deltaTime)
        {
            var addSpeed = 0f;

            if (Keyboard.IsKeyPressed(ConsoleKey.Spacebar))
            {
                addSpeed = Const.PlayerRunSpeed;
            }

            if (Keyboard.IsKeyPressed(ConsoleKey.A))
            {
                _player.Angle += deltaTime * (Const.PlayerRotationSpeed + addSpeed / 2f);
            }
            else if (Keyboard.IsKeyPressed(ConsoleKey.D))
            {
                _player.Angle -= deltaTime * (Const.PlayerRotationSpeed + addSpeed / 2f);
            }

            if (Keyboard.IsKeyPressed(ConsoleKey.W))
            {
                var speed = deltaTime * (Const.PlayerWalkingSpeed + addSpeed);
                var force = new Vector2(MathF.Sin(_player.Angle) * speed, MathF.Cos(_player.Angle) * speed);
                _player.Position += force;

                var colission = _level.GetCell(_player.Position);
                if (Const.ColissionBlocks.Contains(colission))
                {
                    force = new Vector2(MathF.Sin(_player.Angle) * speed, MathF.Cos(_player.Angle) * speed);
                    _player.Position -= force;
                }
            }
            else if (Keyboard.IsKeyPressed(ConsoleKey.S))
            {
                var speed = deltaTime * (Const.PlayerWalkingSpeed + addSpeed);
                var force = new Vector2(MathF.Sin(_player.Angle) * speed, MathF.Cos(_player.Angle) * speed);
                _player.Position -= force;

                var colission = _level.GetCell(_player.Position);
                if (Const.ColissionBlocks.Contains(colission))
                {
                    force = new Vector2(MathF.Sin(_player.Angle) * speed, MathF.Cos(_player.Angle) * speed);
                    _player.Position += force;
                }
            }
        }
    }
}

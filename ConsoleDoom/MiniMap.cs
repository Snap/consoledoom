﻿using System.Numerics;

namespace ConsoleDoom
{
    /// <summary>
    /// Draw mini map.
    /// </summary>
    public class MiniMap
    {
        private Level _level;
        private readonly Screen _screen;
        private readonly Player _player;
        private readonly int _height;
        private readonly int _width;
        private char[,] _mapBuffer;

        public MiniMap(Screen screen, Player player, int width, int height)
        {
            _screen = screen;
            _player = player;
            _width = width;
            _height = height;
        }

        public void Init(Level level)
        {
            _level = level;
            _mapBuffer = new char[_level.Width, _level.Height];
        }

        public void SetMarker(Vector2 position, char markerChar)
        {
            _mapBuffer[(int)position.X, (int)position.Y] = markerChar;
        }

        public void Update()
        {
            const int padding = 1;

            // Draw level in minimap

            var px = (int)_player.Position.X - _width / 2;
            var py = (int)_player.Position.Y - _height / 2;

            for (int y = 0; y < _height; y++)
            {
                for (int x = 0; x < _width; x++)
                {
                    char pixel;

                    if (px + x >= _mapBuffer.GetLength(0) 
                        || py + y >= _mapBuffer.GetLength(1)
                        || px + x < 0
                        || py + y < 0)
                    {
                        pixel = Const.MiniMapOutsideMarker;
                    }
                    else
                    {
                        pixel = _mapBuffer[px + x, py + y];
                    }

                    _screen.SetPixel(x + padding + 1, y + padding + 1, pixel);
                }
            }

            // Draw border corners

            _screen.SetPixel(padding, padding, Const.MiniMapBorderCorners[0]); // left top
            _screen.SetPixel(_width + padding + 1, padding, Const.MiniMapBorderCorners[1]); // right top
            _screen.SetPixel(padding, _height + padding + 1, Const.MiniMapBorderCorners[2]); // left down
            _screen.SetPixel(_width + padding + 1, _height + padding + 1, Const.MiniMapBorderCorners[3]); // right down

            // Draw border lines

            for (int x = 1; x <= _width; x++)
                _screen.SetPixel(padding + x, padding, Const.MiniMapBorderHorizontal); // top

            for (int y = 1; y <= _height; y++)
                _screen.SetPixel(_width + padding + 1, padding + y, Const.MiniMapBorderVertical); // right

            for (int x = 1; x <= _width; x++)
                _screen.SetPixel(padding + x, _height + padding + 1, Const.MiniMapBorderHorizontal); // down

            for (int y = 1; y <= _height; y++)
                _screen.SetPixel(padding, padding + y, Const.MiniMapBorderVertical); // left

            // Draw player position in center map

            var centerX = padding + 1 + _width / 2;
            var centerY = padding + 1 + _height / 2;
            _screen.SetPixel(centerX, centerY, Const.MiniMapPlayerMarker);

            // refresh char buffer
            _level.CopyTo(_mapBuffer);
        }
    }
}
